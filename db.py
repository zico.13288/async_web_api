from gino import Gino
import asyncio


db = Gino()


class Ticket(db.Model):
    __tablename__ = 'tickets'

    number = db.Column(db.Integer(), primary_key=True)
    date = db.Column(db.Date())
    time = db.Column(db.Time())

Ticket()
# if __name__ == '__main__':
#     asyncio.get_event_loop().run_until_complete()
