from aiohttp import web
import aiohttp_jinja2
import jinja2
from asyncpg import UniqueViolationError
from datetime import datetime

from db import db, Ticket

app = web.Application()


async def main():
    await db.set_bind('postgresql://postgres:1@localhost:5432/')

aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))


@aiohttp_jinja2.template("index.html")
async def index(request):
    return


async def add(request):
    if len(request.query) == 3:
        data = request.query
        try:
            if data['number'].isdigit() or len(data['number']) == 3:
                await db.set_bind('postgresql://postgres:1@localhost:5432/')
                await Ticket.create(number=int(data['number']), date=datetime.strptime(data['date'], "%Y-%m-%d"),
                                    time=datetime.strptime(data['time'], "%H:%M"))
                await db.pop_bind().close()
                return web.Response(text="Запрос получен")
            else:
                return web.Response(text="Номер должен состоять из цифр из трех цисел")
        except UniqueViolationError:
            return web.Response(text="Такой билет уже есть в базе")
    else:
        return web.Response(text="Введите правильный запрос")


app.add_routes([web.get('/', index), web.get('/add', add)])

if __name__ == '__main__':
    web.run_app(app)